//
// A basic package that facilitates interactions with the Cyclops
// customer db - these types are used within the customer DB itself
// as well as clients of the customer DB.
//
// (C) Cyclops Labs 2019
//

package customerdbclient

import (
	"bytes"
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"

	l "gitlab.com/cyclops-utilities/logging"
	"gitlab.com/cyclops-utilities/types"
)

// CustomerDBService contains the endpoint where the customer db service lives...
// It is implemented as an interface which includes a PostCommand...
type CustomerDBService struct {
	Endpoint string
}

// Creates a Reseller in the customer DB; note that it is assumed that the
// reseller contains a set of customers and that these can be marshalled
// correctly.
func (s *CustomerDBService) CreateReseller(r types.Reseller) (success bool, err error) {
	val, _ := json.Marshal(r)
	l.Debug.Printf("r= %+v\n", string(val))
	newCustomerEndpoint := s.Endpoint + "/reseller/"
	req, err := http.NewRequest("POST", newCustomerEndpoint, bytes.NewBuffer(val))
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)

	if err != nil {
		//panic(err)
		l.Error.Printf("error = %v\n", err.Error())
		success = false
		return
	}

	//TODO(murp) - fixme...)
	defer resp.Body.Close()

	if err == nil {
		if (resp.StatusCode != http.StatusCreated) && (resp.StatusCode != http.StatusAccepted) {
			l.Warning.Printf("Unexpected response Creating new Reseller record - Response Status: %v\n", resp.Status)
			body, _ := ioutil.ReadAll(resp.Body)
			l.Debug.Printf("Response Body: %v\n", string(body))
			success = false
		} else {
			// this is the successful case - only dump output if in debug mode as this
			// should be the default...
			body, _ := ioutil.ReadAll(resp.Body)
			l.Debug.Printf("Sending Cyclops Customer DB service - Response Status: %v, Response Body: %v\n", resp.Status, string(body))
			success = true
		}
	}
	return
}

// PutProduct puts a product under a given reseller - it takses as input the product
// which includes the customer id and posts it into the customer entry in the customerdb
// accordingly
func (s *CustomerDBService) PutProduct(p types.Product) (err error) {
	val, _ := json.Marshal(p)
	l.Debug.Printf("r= %+v\n", string(val))
	productEndpoint := s.Endpoint + "/product/" + p.ProductId
	req, err := http.NewRequest("PUT", productEndpoint, bytes.NewBuffer(val))
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)

	if err != nil {
		//panic(err)
		l.Error.Printf("error = %v\n", err.Error())
		return
	}

	//TODO(murp) - fixme...)
	defer resp.Body.Close()

	if err == nil {
		if (resp.StatusCode != http.StatusCreated) && (resp.StatusCode != http.StatusAccepted) {
			l.Warning.Printf("Unexpected response Creating new Product record - Response Status: %v\n", resp.Status)
			body, _ := ioutil.ReadAll(resp.Body)
			l.Debug.Printf("Response Body: %v\n", string(body))
			err = errors.New("Unexpected response posting product to customerdb")
		} else {
			// this is the successful case - only dump output if in debug mode as this
			// should be the default...
			body, _ := ioutil.ReadAll(resp.Body)
			l.Debug.Printf("Sending Cyclops Customer DB service - Response Status: %v, Response Body: %v\n", resp.Status, string(body))
		}
	}
	return
}
